import numpy as np
from scipy import sparse
import csv
from sklearn import svm
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn.svm import SVC
from sklearn.linear_model import Perceptron
from sklearn.neighbors import KNeighborsClassifier
import sys
from docutils.parsers.rst.directives import encoding

genderFile = open('gender.csv', encoding='utf-8')
genderReader = csv.reader(genderFile)

next(genderReader)


Y = []


X = []


for row in genderReader:
   Y.append(row[0])
   X.append(row[1:])


genderFile.close()


X_len = len(X)
for row in range(X_len):
    X[row][0] = float(X[row][0])
    X[row][1] = float(X[row][1])
    X[row][2] = float(X[row][2])
 
 

clf_LinearSVC = svm.LinearSVC()
clf_tree = tree.DecisionTreeClassifier()

clf_LinearSVC = clf_LinearSVC.fit(X, Y)
clf_tree.fit(X, Y)
 

acc_LinearSVC = accuracy_score(Y, clf_LinearSVC.predict(X)) * 100.0
pred_tree = clf_tree.predict(X)
acc_tree = accuracy_score(Y, pred_tree) * 100
print(acc_tree)
 
index = np.argmax([acc_LinearSVC, acc_tree])
classifiers = {0: 'LinearSVC', 1: 'DecisionTree'}
best_classifier = classifiers[index]
print('Best girl classifier is {}'.format(best_classifier))




Round1 = input('Nhập số đo vòng 1 (cm)')
Round2 = input('Nhập số đo vòng 2 (cm)')
Round3 = input('Nhập só đo vòng 3 (cm)')


try:
    Round1 = float(Round1)
    Round2 = float(Round2)
    Round3 = float(Round3)
except ValueError:
    print("Nhập không hợp lệ")
    sys.exit()
   
if Round1 <= 0.0 or Round2 <= 0.0 or Round3 <= 0.0:
    print('Nhập không hợp lệ')
    sys.exit()


index = 0
pred = clf_tree.predict([[Round1, Round2, Round3]])
Label = pred[0]
validation = input('%s' % Label)

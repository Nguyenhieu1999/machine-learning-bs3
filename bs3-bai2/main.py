import cv2
import sys

imagePath = sys.argv[1]

image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.3,
    minNeighbors=2,
    minSize=(30, 30)
)

print("Tìm thấy {0} mặt.".format(len(faces)))

for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 1)
    roi_color = image[y:y + h, x:x + w]
    print("Mặt được tìm thấy, lưu vào máy.")
    cv2.imwrite(str(w) + str(h) + '_faces.jpg', roi_color)

status = cv2.imwrite('anh_xac_dinh_mat.jpg', image)
print("Ảnh xác định khuôn mặt được lưu vào máy ", status)
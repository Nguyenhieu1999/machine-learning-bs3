import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt

df = pd.read_csv("BBC News Train.csv")
df.head()

df['category_id'] = df['Category'].factorize()[0]# Liên kết tên danh mục với chỉ mục số và lưu nó trong cột mới category_id
df['category_id'][0:10]

category_id_df = df[['Category', 'category_id']].drop_duplicates().sort_values('category_id')

#Tạo từ điển
category_to_id = dict(category_id_df.values)
id_to_category = dict(category_id_df[['category_id', 'Category']].values)

print(id_to_category)

df.sample(5, random_state=0)
df.groupby('Category').category_id.count()

df.groupby('Category').category_id.count().plot.bar(ylim=0)
plt.show()

#Đếm tần suất xuất hiện của từ
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2), stop_words='english')
features = tfidf.fit_transform(df.Text).toarray() 
labels = df.category_id    
features.shape
# Ghi nhớ từ điển được tạo để ánh xạ tên danh mục thành một số
category_to_id.items()
# Hàm được sắp xếp Chuyển đổi các mục từ điển thành một danh sách (được sắp xếp).
sorted(category_to_id.items())


from sklearn.feature_selection import chi2

N = 3  # Tìm kiếm 3 danh mục top đầu

#Với mỗi danh mục tìm từ có độ tương quan
for Category, category_id in sorted(category_to_id.items()):
  features_chi2 = chi2(features, labels == category_id)                  
  indices = np.argsort(features_chi2[0])# Sắp xếp các chỉ số của tính năng chi2 [0] - số liệu thống kê chi bình phương của từng tính năng                                
  feature_names = np.array(tfidf.get_feature_names())[indices]           
  unigrams = [v for v in feature_names if len(v.split(' ')) == 1]#Các từ đơn có độ tương quan          
  bigrams = [v for v in feature_names if len(v.split(' ')) == 2]#Các từ ghép có độ tương quan         
  print("# '{}':".format(Category))
  print("  . Most correlated unigrams:\n       . {}".format('\n       . '.join(unigrams[-N:])))
  print("  . Most correlated bigrams:\n       . {}".format('\n       . '.join(bigrams[-N:]))) 
  
from sklearn.manifold import TSNE

SAMPLE_SIZE = int(len(features) * 0.3)
np.random.seed(0)
indices = np.random.choice(range(len(features)), size=SAMPLE_SIZE, replace=False)          
projected_features = TSNE(n_components=2, random_state=0).fit_transform(features[indices]) 


colors = ['pink', 'green', 'midnightblue', 'orange', 'darkgrey']
for category, category_id in sorted(category_to_id.items()):
    points = projected_features[(labels[indices] == category_id).values]
    plt.scatter(points[:, 0], points[:, 1], s=30, c=colors[category_id], label=category)
plt.title("Tần suất xuất hiện của từ, hiện thị 2 chiều",
          fontdict=dict(fontsize=15))
plt.legend()
plt.show()


from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_score

models = [
    RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0),
    MultinomialNB(),
    LogisticRegression(random_state=0),
]

CV = 5  # Xác thực chéo với 5 lần gấp 20% dữ liệu khác nhau (chia 80-20 lần với 5 lần)

# Tạo khung dữ liệu sẽ lưu trữ kết quả cho cả 5 thử nghiệm của 3 mô hình khác nhau
cv_df = pd.DataFrame(index=range(CV * len(models)))
entries = []


for model in models:
  model_name = model.__class__.__name__
  # Tạo 5 mô hình với các bộ kiểm tra 20% khác nhau và lưu trữ độ chính xác của chúng
  accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
  # Nối tất cả 5 độ chính xác vào danh sách mục nhập (sau khi cả 3 mô hình được chạy, sẽ có 3x5 = 15 mục)
  for fold_idx, accuracy in enumerate(accuracies):
    entries.append((model_name, fold_idx, accuracy))

cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])


import seaborn as sns

sns.boxplot(x='model_name', y='accuracy', data=cv_df)
sns.stripplot(x='model_name', y='accuracy', data=cv_df, 
              size=8, jitter=True, edgecolor="gray", linewidth=2)
plt.show()

print(cv_df.groupby('model_name').accuracy.mean())


from sklearn.model_selection import train_test_split

model = LogisticRegression(random_state=0)
X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)
y_pred_proba = model.predict_proba(X_test)
y_pred = model.predict(X_test)


from sklearn.metrics import confusion_matrix
import seaborn as sns

conf_mat = confusion_matrix(y_test, y_pred)
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=category_id_df.Category.values, yticklabels=category_id_df.Category.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

model.fit(features, labels)


from sklearn.feature_selection import chi2
N = 5
for Category, category_id in sorted(category_to_id.items()):
  indices = np.argsort(model.coef_[category_id])   # Sử dụng các đồng số / trọng số của mô hình
  feature_names = np.array(tfidf.get_feature_names())[indices]
  unigrams = [v for v in reversed(feature_names) if len(v.split(' ')) == 1][:N]
  bigrams = [v for v in reversed(feature_names) if len(v.split(' ')) == 2][:N]
  print("# '{}':".format(Category))
  print("  . Top unigrams:\n       . {}".format('\n       . '.join(unigrams)))
  print("  . Top bigrams:\n       . {}".format('\n       . '.join(bigrams)))
  
  
texts = ["Hooli stock price soared after a dip in PiedPiper revenue growth.",
         "Captain Tsubasa scores a magnificent goal for the Japanese team.",
         "Merryweather mercenaries are sent on another mission, as government oversight groups call for new sanctions.",
         "Beyoncé releases a new album, tops the charts in all of south-east Asia!",
         "You won't guess what the latest trend in data analysis is!"]
text_features = tfidf.transform(texts)
predictions = model.predict(text_features)
for text, predicted in zip(texts, predictions):
  print('"{}"'.format(text))
  print("  - Danh mục dự đoán: '{}'".format(id_to_category[predicted]))
  print("")


test_df = pd.read_csv("BBC News Test.csv")
test_features = tfidf.transform(test_df.Text.tolist())
Y_pred = model.predict(test_features)
print(Y_pred)

Y_pred_name =[]
for cat_id in Y_pred :
    Y_pred_name.append(id_to_category[cat_id])

print(Y_pred_name)


Result = pd.DataFrame({
        "ArticleId": test_df["ArticleId"],
        "Category": Y_pred_name
    })
Result.to_csv('Ketquaphanloai.csv', index=False)
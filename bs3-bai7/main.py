from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import decomposition, ensemble
from pyvi import ViTokenizer, ViPosTagger
from tqdm import tqdm
import numpy as np
import gensim
import numpy as np

def preprocessing_doc(doc):
    lines = gensim.utils.simple_preprocess(doc)
    lines = ' '.join(lines)
    lines = ViTokenizer.tokenize(lines)

    return lines

import pickle

X_data = pickle.load(open('data/X_data.pkl', 'rb'))
y_data = pickle.load(open('data/y_data.pkl', 'rb'))

# X_test = pickle.load(open('data/X_test.pkl', 'rb'))
# y_test = pickle.load(open('data/y_test.pkl', 'rb'))

#Chọn số từ tối đa là 30000
tfidf_vect = TfidfVectorizer(analyzer='word', max_features=30000)
tfidf_vect.fit(X_data) # learn vocabulary and idf from training set
X_data_tfidf =  tfidf_vect.transform(X_data)

from sklearn.decomposition import TruncatedSVD
svd = TruncatedSVD(n_components=300, random_state=42)
svd.fit(X_data_tfidf)
X_data_tfidf_svd = svd.transform(X_data_tfidf)
encoder = preprocessing.LabelEncoder()
y_data_n = encoder.fit_transform(y_data)

print(encoder.classes_)

from sklearn.model_selection import train_test_split
def train_model(classifier, X_data, y_data, X_test=None, y_test=None, is_neuralnet=False, n_epochs=3):       
    X_train, X_val, y_train, y_val = train_test_split(X_data, y_data, test_size=0.1, random_state=42)
    
    if is_neuralnet:
        classifier.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=n_epochs, batch_size=512)
        
        val_predictions = classifier.predict(X_val)
        test_predictions = classifier.predict(X_test)
        val_predictions = val_predictions.argmax(axis=-1)
#         test_predictions = test_predictions.argmax(axis=-1)
    else:
        classifier.fit(X_train, y_train)
    
        train_predictions = classifier.predict(X_train)
        val_predictions = classifier.predict(X_val)
#         test_predictions = classifier.predict(X_test)
        
    print("Validation accuracy: ", metrics.accuracy_score(val_predictions, y_val))
#     print("Test accuracy: ", metrics.accuracy_score(test_predictions, y_test))

model = linear_model.LogisticRegression()
model.fit(X_data_tfidf, y_data)
# model = naive_bayes.MultinomialNB()
train_model(model, X_data_tfidf, y_data, is_neuralnet=False)

test_doc = '''Nói về khía cạnh tâm lý trong những cú sút phạt trực tiếp, việc đạt hiệu suất thành bàn quá thấp chắc chắn sẽ có ảnh hưởng tới sự tự tin của Ronaldo. 
Có lẽ vì thế mà siêu sao Bồ Đào Nha đã cố gắng thử nghiệm 1 số thay đổi trong kỹ thuật sút phạt nhưng lại chưa mang tới thành công.
Đôi khi chỉ 1 chút lưỡng lự, phân tâm cũng ảnh hưởng lớn tới cú sút và khiến chúng không đạt hiệu quả cao nhất.'''
test_doc = preprocessing_doc(test_doc)
# test_vec = get_word2vec_data([test_doc])

test_doc_tfidf = tfidf_vect.transform([test_doc])
print(np.shape(test_doc_tfidf))
test_doc_svd = svd.transform(test_doc_tfidf)

model.predict(test_doc_tfidf)
